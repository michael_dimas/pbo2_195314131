package com.mycompany.praktikum1b;

public class MasyarakatSekitar extends Penduduk {

    private int nomor;

    public MasyarakatSekitar() {

    }

    public MasyarakatSekitar(int nomor, String nama, String tempatTanggalLahir) {
        super(nama, tempatTanggalLahir);
        this.nomor = nomor;
    }

    public void setNomor(int nomor) {
        this.nomor = nomor;
    }

    public int getNomor() {
        return nomor;
    }

    @Override
    public double hitungIuran() {
        double hitungIuran;
        return getNomor() * 100;
    }
}
