package com.mycompany.praktikum1b;

public abstract class Penduduk {

    protected String nama;
    protected String tempatTanggalLahir;

    public Penduduk() {

    }

    public Penduduk(String nama, String tempatTanggalLahir) {
        this.nama = nama;
        this.tempatTanggalLahir = tempatTanggalLahir;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setTempatTanggalLahir(String tempatTanggalLahir) {
        this.tempatTanggalLahir = tempatTanggalLahir;
    }

    public String getTempatTanggalLahir() {
        return tempatTanggalLahir;
    }

    public abstract double hitungIuran();
}
