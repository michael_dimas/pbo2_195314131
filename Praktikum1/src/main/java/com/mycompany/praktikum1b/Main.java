package com.mycompany.praktikum1b;

public class Main {

    public static void main(String[] args) {
        UKM ukm = new UKM();
        Penduduk[] anggota;
        anggota = new Penduduk[10];
        Mahasiswa ketua = new Mahasiswa();
        Mahasiswa sekretaris = new Mahasiswa();
        int x = 0;

        System.out.println("Unit Kegiatan Mahasiswa(UKM)");
        System.out.println("====================================================");

        ketua.setNama("Michael");
        ketua.setNim(195314131);
        ketua.setTempatTanggalLahir("Sukabumi, 2 November 2001");

        sekretaris.setNama("Dimas");
        sekretaris.setNim(195314132);
        sekretaris.setTempatTanggalLahir("Sukabumi, 3 November 2002");

        ukm.setKetua(ketua);
        ukm.setSekretaris(sekretaris);
        ukm.setNamaUnit("Karate");

        System.out.println(ukm.getNamaUnit());
        System.out.println("====================================================");
        System.out.println("Ketua\t\t\t : " + ukm.getKetua().getNama());
        System.out.println("NIM\t\t\t : " + ketua.getNim());
        System.out.println("Tempat Tanggal Lahir : " + ketua.getTempatTanggalLahir());
        System.out.println("");
        System.out.println("Sekretaris\t\t : " + ukm.getSekretaris().getNama());
        System.out.println("NIM\t\t\t : " + sekretaris.getNim());
        System.out.println("Tempat Tanggal Lahir\t : " + sekretaris.getTempatTanggalLahir());
        System.out.println("====================================================");

        anggota[x] = new MasyarakatSekitar();
        ((MasyarakatSekitar) anggota[x]).setNama("Surya");
        ((MasyarakatSekitar) anggota[x]).setNomor(201);
        ((MasyarakatSekitar) anggota[x]).setTempatTanggalLahir("Sukabumi, 4 November 2001");
        x++;

        anggota[x] = new Mahasiswa();
        ((Mahasiswa) anggota[x]).setNama("Abdi");
        ((Mahasiswa) anggota[x]).setNim(195314133);
        ((Mahasiswa) anggota[x]).setTempatTanggalLahir("Sukabumi, 5 November 2000");
        x++;

        System.out.println("Anggota UKM Karate");
        for (int i = 0; i < x; i++) {
            if (anggota[i] instanceof MasyarakatSekitar) {
                System.out.println("Masyarakat Sekitar");
                System.out.println("Nama\t\t : " + anggota[i].getNama());
                System.out.println("NIM\t\t : " + ((MasyarakatSekitar) anggota[i]).getNomor());
                System.out.println("Tempat Tanggal Lahir : " + anggota[i].getTempatTanggalLahir());
                System.out.println("Iuran UKM\t\t : " + ((MasyarakatSekitar) anggota[i]).hitungIuran());
                System.out.println("");
            } else {
                System.out.println("Mahasiswa");
                System.out.println("Nama\t\t\t : " + anggota[i].getNama());
                System.out.println("NIM\t\t\t : " + ((Mahasiswa) anggota[i]).getNim());
                System.out.println("Tempat Tanggal Lahir : " + anggota[i].getTempatTanggalLahir());
                System.out.println("Iuran UKM\t : " + ((Mahasiswa) anggota[i]).hitungIuran());
            }
        }
    }
}
